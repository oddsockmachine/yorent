<<<<<<< HEAD
=======
# A thin ORM to make it easier to work with the database
>>>>>>> b604e730314058700b693e99f5a2e960a0c0ee95
from mongoengine import *
from datetime import datetime, timedelta
from random import randint
from pprint import pprint
import os
import json

connect('yorent')


class BaseReview(Document):
    meta = {'abstract': True}
    # Title for this review
    title = StringField(required=True)
    # Body text of the review
    body = StringField(required=True)
    # Timestamp when the review was made
    timestamp = DateTimeField()
    # User who made this review
    author = ReferenceField('User')
    # list of users who upvoted this review
    # use length of this list to calculate up-ness
    upvoters = ListField(ReferenceField('User'))
    downvoters = ListField(ReferenceField('User'))
    # Rank - ie: upvotes minus downvotes
    rank = IntField()
    # Year - the time perioud this review applies to
    year = IntField()

    def upvote(self, user):
        """Add this user to list of upvoters, if valid"""
        if user in self.upvoters:
            # Return an error, can't allow user to upvote twice
            return "error - user has already upvoted"
        if user in self.downvoters:
            # User has changed their mind, remove from old list
            self.downvoters = [v for v in self.downvoters if v!=user]
            self.save()
        # Add user to appropriate list
        self.upvoters.append(user)
        self.save()
        return "ok"

    def downvote(self, user):
        """Add this user to list of downvoters, if valid"""
        if user in self.downvoters:
            return "error - user has already downvoted"
        if user in self.upvoters:
            # User has changed their mind, remove from old list
            self.upvoters = [v for v in self.upvoters if v!=user]
            self.save()
        # Add user to appropriate list
        self.downvoters.append(user)
        self.save()
        return "ok"

    def get_up_down_nums(self):
        """Return a strings with lengths of up, down votes, separated by space.
        Calculate review rank while here, since it's convenient"""
        ups = len(self.upvoters)
        downs = len(self.downvoters)
        self.rank = ups - downs
        self.save()
        return str(ups)+" "+str(downs)

class HouseReview(BaseReview):

    # House that is being reviewed
    house = ReferenceField('House')
    #TODO pros = ""/[], cons = ""/[]
    # pros = ListField(StringField())
    # cons = ListField(StringField())
    # Rating of this house in stars
    rating = IntField()
    # # Date/period to which this review relates - maybe just mm/yy
      # date = DateTimeField()

    def create(self, title, body, timestamp, house, rating, author, year):
        self.title = title
        self.body = body
        self.timestamp = timestamp
        self.author = author
        self.house = house
        self.rating = int(rating)
        self.rank = 0
        self.year = year
        self.save()
        house.reviews.append(self)  # note: this must be done after self.save
        house.save()
        return self

  #  def __repr__(self):
   #     return "{}* review of {} by {}".format(self.rating, self.house.name, self.author)

    def calc_average(self):
        """Calculate average reviews for self.house.average_rating,
        self.house.neighbourhood.average_house_rating and
        self.landlord.average_house_rating"""
        # Calculate self.house.average_rating
        total = 0
        num = len(self.house.reviews)
        for _rev in self.house.reviews:
            total += _rev.rating
        self.house.average_rating = total/float(num)
        self.house.save()

        # Neighbourhood
        # We take the average of all houses average scores, not the average of all houses reviews
        # (to avoid houses with more reviews from having greater weight)
        total = 0
        nhood = self.house.neighbourhood
        num = len(nhood.houses)
        for _house in nhood.houses:
            total += _house.average_rating if _house.average_rating else 0
        nhood.average_house_rating = total/float(num)
        nhood.save()

        # Landlord

        total = 0
        llord = self.house.landlord
        num = len(llord.houses)
        for _house in llord.houses:
            total += _house.average_rating if _house.average_rating else 0
        llord.average_house_rating = total/float(num)
        llord.save()
        return




class House(Document):
    # Name of this house/address eg: 16 Lavender Drive
    name = StringField(required=True)
    # Brief overview eg: "3 bed student house, off street parking, Hes Rd"
    overview = StringField(required=True)
    # Location of this house eg: Fulford - York
    # Holds a cached string representation of hood/town, to avoid looking up every time
    neighbourhood = ReferenceField('Neighbourhood')
    # Postcode of this house - used to calculate neighbourhood and town/city
    postcode = StringField(required=True)
    # Landlord/agent of this house
    landlord = ReferenceField('Landlord')
    # List of reviews of this house
    reviews = ListField(ReferenceField('HouseReview', reverse_delete_rule=PULL))
    # Average rating in stars (0-5)
    average_rating = FloatField()
    # list of urls of images
    images = ListField(StringField())
    # Town/City this house is in.
    town = ReferenceField('Town')
    # Neighbourhood this house is in.
    neighbourhood = ReferenceField('Neighbourhood')
    # Timestamp when the house was added
    date_added = DateTimeField()
    # Which user added this house - usually not needed, except to ban spammers
    added_by_user = ReferenceField('User')
    # Latitude and Longitude, for showing on maps
    latitude = FloatField()
    longitude = FloatField()
    # "At a glance" details about the house
    num_bedrooms = IntField()
    num_bathrooms = IntField()
    

    def create(self, name, postcode, overview, town, landlord, nhood, lat, lng, user):
        self.name = name
        self.postcode = postcode
        self.overview = overview
        self.town = town
        self.neighbourhood = nhood
        self.landlord = landlord
        self.latitude = lat
        self.longitude = lng
        self.added_by_user = user
        self.num_bedrooms = 0
        self.num_bathrooms = 0
        self.save()
        town.houses.append(self)
        town.save()
        landlord.houses.append(self)
        landlord.save()
        nhood.houses.append(self)
        nhood.save()
        return self

    def has_user_already_reviewed(self, user):
        for rev in self.reviews:
            if rev.author == user:
                return True
        return False

    def __repr__(self):
        return "<%s in %s, %s>" % (self.name, self.neighbourhood.name, self.town.name)


class LandlordReview(BaseReview):
    # Landlord that is being reviewed
    landlord = ReferenceField('Landlord')
    # Rating of this house in stars
    rating = IntField()
    # Date/period to which this review relates - maybe just mm/yy
    date = DateTimeField()

    def create(self, title, body, timestamp, landlord, rating, author, year):
        self.title = title
        self.body = body
        self.timestamp = timestamp
        self.author = author
        self.landlord = landlord
        self.rating = int(rating)
        self.rank = 0
        self.year = year
        self.save()
        landlord.reviews.append(self)
        landlord.save()
        return self

    def calc_average(self):
        """Calculate average review for self.landlord.average_review_rating"""
        total = 0
        num = len(self.landlord.reviews)
        for _rev in self.landlord.reviews:
            total += int(_rev.rating)
        av = total/float(num)
        self.landlord.average_review_rating = av
        self.landlord.save()
        return

#    def __repr__(self):
 #       return "<%s' review of %s>" % (self.author, self.landlord.name)




class Landlord(Document):
    # Name of this landlord eg: YRLA
    name = StringField()
    # Website of landlord or agent
    website = StringField()
    # Houses owned by landlord
    houses = ListField(ReferenceField('House', reverse_delete_rule=PULL))
    # Which town does this landlord operate in
    town = ReferenceField('Town')
    # Average rating for landlord, in *.*/5
    average_review_rating = FloatField()
    # Average rating for landlord's houses, in *.*/5
    average_house_rating = FloatField()
    # List of reviews of this landlord
    reviews = ListField(ReferenceField('LandlordReview', reverse_delete_rule=PULL))
    # The landlord's user account - so they can potentially log in an make changes
    owner_account = ReferenceField('User')
    # Which user added this house - usually not needed, except to ban spammers
    added_by_user = ReferenceField('User')
    
    def create(self, name, website, town, user):
        self.name = name
        self.website = website
        self.town = town
        self.added_by_user = user
        self.save()
        town.landlords.append(self)
        town.save()
        return self

    def has_user_already_reviewed(self, user):
        for rev in self.reviews:
            if rev.author == user:
                return True
        return False


    def __repr__(self):
        return '<Landlord %s in %s>' % (self.name, self.town.name)


class Neighbourhood(Document):
    # neighbourhood name
    name = StringField(required=True)
    # Town/City this neighbourhood is in.
    town = ReferenceField('Town')
    # All the houses in this neighbourhood (list)
    houses = ListField(ReferenceField('House', reverse_delete_rule=PULL))
    # Average rating of all houses in this neighbourhood
    average_house_rating = FloatField()

    def create(self, name, town):
        self.name = name
        self.town = town
        self.save()
        self.save()
        town.neighbourhoods.append(self)
        town.save()
        return self

    def __repr__(self):
        return '<Neighbourhood %s>' % (self.name)


class Town(Document):
    # Town or city name
    name = StringField(required=True)
    # Neighbourhoods in this town (list)
    neighbourhoods = ListField(ReferenceField('Neighbourhood', reverse_delete_rule=PULL))
    # All the houses in this town (list)
    houses = ListField(ReferenceField('House', reverse_delete_rule=PULL))
    # All the landlords in this town (list)
    landlords = ListField(ReferenceField('Landlord', reverse_delete_rule=PULL))
    # image for thumbnail view (url)
    image = StringField()
    #data for showing on map
    lat = StringField()  # latitude
    lng = StringField()  # longitude
    zoom = StringField()  # google maps zoom level
    
    def create(self, name):
        self.name = name
        self.save()
        return self

    def __repr__(self):
        return '<Town %s>' % (self.name)




ROLE_USER = 0
ROLE_MODERATOR = 1
ROLE_ADMIN = 2

class User(Document):
    # Unique nickname/username - shown on pages
    nickname = StringField(required=True)
    # Email address
    # should it use uni addresses (@*.ac.uk) to start with?
    # ensures real people, not trolls, but excludes non-students
    email = StringField(required=True)
    # User or Admin?
    role = IntField()
    # House_Reviews submitted by this user
    house_reviews = ListField(ReferenceField('HouseReview', reverse_delete_rule=PULL))
    # Landlord_Reviews submitted by this user
    landlord_reviews = ListField(ReferenceField('LandlordReview', reverse_delete_rule=PULL))
    #about_me = StringField()
    # When this user joined
    date_joined = DateTimeField()

    def create(self, nickname, email, role):
        self.nickname = nickname
        # validate email - real & not already registered
        self.email = email
        self.role = role
        self.date_joined = datetime.now()
        self.save()
        return self


    # @staticmethod
    # def make_unique_nickname(nickname):
    #     if User.query.filter_by(nickname = nickname).first() == None:
    #         return nickname
    #     version = 2
    #     while True:
    #         new_nickname = nickname + str(version)
    #         if User.query.filter_by(nickname = new_nickname).first() == None:
    #             break
    #         version += 1
    #     return new_nickname


    # def avatar(self, size):
    #     return 'http://www.gravatar.com/avatar/' + md5(self.email).hexdigest() + '?d=mm&s=' + str(size)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.email)

class BlogPost(Document):
    # category of post - eg News, Development
    category = StringField()
    # Title of post
    title = StringField()
    # content
    content = StringField()
    # date and time posted
    timestamp = DateTimeField()

    def create(self, title, category, content):
        self.title = title
        self.category = category
        self.content = content
        self.timestamp = datetime.now()
        self.save()
        return self

