from flask import render_template, request, redirect, url_for, json, g, session, flash
from flask.ext.login import LoginManager, login_user, logout_user, current_user, login_required
from app import app, oid, lm
from pprint import pformat
from datetime import datetime
from requests import get

import model as m
import tasks as t
import forms as f

################################################################################
#################### Users #####################################################
################################################################################

@lm.user_loader
def load_user(user_id):
    user = m.User.objects(id=user_id)
    user = None if len(user) != 1 else user[0]
    return user

@app.route('/login', methods = ['GET', 'POST'])
@oid.loginhandler
def login():
    print "logging in"
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    form = f.LoginForm()
    if form.validate_on_submit():
        print form.remember_me.data
        session['remember_me'] = form.remember_me.data
        print "about to try oid"
        return oid.try_login(form.openid.data, ask_for = ['nickname', 'email'])
    return render_template('login.html',
        title = 'Sign In',
        form = form,
        providers = app.config['OPENID_PROVIDERS'],
        nav = nav())

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.before_request
def before_request():
    g.user = current_user

@oid.after_login
def after_login(resp):
    if resp.email is None or resp.email == "":
        flash('Invalid login. Please try again.')
    user = m.User.objects(email = resp.email)
    user = None if len(user)!=1 else user[0]
    if user is None:
        nickname = resp.nickname
        if nickname is None or nickname == "":
            nickname = resp.email.split('@')[0]
        user = m.User()
        user.create(nickname, resp.email, m.ROLE_USER)
        user.save()
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
    login_user(user, remember = remember_me)
    flash('Logged in successfully.')
    return redirect(request.args.get('next') or url_for('index'))

@app.route('/user/<nickname>')
@login_required
def user(nickname):
    user = m.User.objects.get(nickname = nickname)
    if user == None:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('index'))
    h_reviews = m.HouseReview.objects(author=user)
    ll_reviews = m.LandlordReview.objects(author=user)
    print h_reviews
    print ll_reviews
    return render_template('user.html',
        user = user,
        h_reviews=h_reviews,
        ll_reviews=ll_reviews,
        nav=nav())

################################################################################
#################### Towns #####################################################
################################################################################

@app.route("/towns")
def towns():
    if request.args.get("town_id"):
        town_id = request.args.get("town_id")
        this_town = m.Town.objects.get(id=town_id)
        return render_template("town_one.html", town=this_town, nav=nav(cur_town=this_town))
    towns = m.Town.objects()
    return render_template("towns.html", nav=nav(), towns=towns)


@app.route("/neighbourhoods")
def neighbourhoods():
    if request.args.get("nhood_id"):
        nhood_id = request.args.get("nhood_id")
        this_nhood = m.Neighbourhood.objects.get(id=nhood_id)
        return render_template("nhood_one.html", nhood=this_nhood, nav=nav(cur_town = this_nhood.town, cur_hood=this_nhood))
    # No neighbourhood specified - show a message?
    return redirect(url_for("houses"))


################################################################################
#################### Houses ####################################################
################################################################################


@app.route("/houses")
def houses():
    if request.args.get("house_id"):
        house_id = request.args.get("house_id")
        this_house = m.House.objects.get(id=house_id)
        user_already_reviewed = False  # Default to false
        if g.user.is_active():  # If user is signed in
            this_user = m.User.objects().get(id=g.user.id)  # get user object
            user_already_reviewed = this_house.has_user_already_reviewed(this_user)
        return render_template("house_one.html",
                               house=this_house,
                               already_reviewed=user_already_reviewed,
                               nav=nav(cur_town=this_house.town, cur_hood=this_house.neighbourhood))
    all_houses = m.House.objects()
    return render_template("houses.html", houses=all_houses, nav=nav())

@app.route("/add_house", methods=['GET', 'POST'])
@login_required
def add_house():
    if request.method == 'POST':
        name = request.values.get("name")
        postcode = request.values.get("pcode")
        overview = request.values.get("oview")
        landlord_id = request.values.get("ll_id")
        landlord_name = request.values.get("ll_name")
        landlord_website = request.values.get("ll_website")
        print landlord_id
        print landlord_name
        print landlord_website
        town = request.values.get("town")
        nhood = request.values.get("nhood")
        lat = float(request.values.get("lat"))
        lng = float(request.values.get("lng"))
        print pformat(request.values)
        print name, postcode, overview, landlord_id, town, nhood
        town_obj = m.Town.objects(name=town)[0]
        print town_obj.name
        this_user = m.User.objects().get(id=g.user.id)
        if landlord_id != "new":  # landlord already exists
            landlord_obj = m.Landlord.objects(id=landlord_id)[0]  # so get from db
        else:  # Create a new one using data from form
            print "creating new landlord"
            landlord_obj = m.Landlord()
            landlord_obj.create(landlord_name, landlord_website, town_obj, this_user)
            print landlord_obj
        print landlord_obj.name
        nhood_obj = m.Neighbourhood.objects(name=nhood, town=town_obj)
        if len(nhood_obj)==0:
            # Create a new nhood
            print "Creating new nhood"
            nhood_obj = m.Neighbourhood()
            nhood_obj.create(nhood, town_obj)
            nhood_obj.save()
        else:
            nhood_obj = nhood_obj[0]
        print nhood_obj.name
        new_house = m.House()
        print lat, lng
        new_house.create(name, postcode, overview, town_obj, landlord_obj, nhood_obj, lat, lng, this_user)
        new_house.save()
        return (url_for("houses", house_id=new_house.id))  # js on page handles redirect
    return render_template("house_new.html", towns=m.Town.objects(), landlords=m.Landlord.objects(), nav=nav())

@app.route("/add_house_review", methods=['GET', 'POST'])
@login_required
def add_house_review():
    if request.method == 'POST':
        this_author = m.User.objects().get(id=g.user.id)
        house_id = request.form.get("review_house_id")
        this_house = m.House.objects.get(id=house_id)
        # Check that this user hasn't already reviewed this house.
        # Users can only review/vote once per object
        if this_house.has_user_already_reviewed(this_author):
            return "Error - You have already reviewed this house.\nPlease edit your original review instead."
        title = request.form.get("review_title")
        body = request.form.get("review_body")
        timestamp = datetime.now()

        year = request.form.get("review_year")
        rating = request.form.get("review_rating")

        new_review = m.HouseReview()

        new_review.create(title, body, timestamp, this_house, rating, this_author, year)
        new_review.save()
        new_review.calc_average()
        this_house.save()
        return (url_for("houses", house_id=house_id))
    house_id = request.args.get("house_id")
    house = m.House.objects.get(id=house_id)
    return render_template("house_review_new.html", nav=nav(cur_town=house.town, cur_hood=house.neighbourhood), house=house)


@app.route("/add_house_image", methods=['GET', 'POST'])
def add_house_image():
    return redirect(url_for("houses"))


################################################################################
#################### Landlords #################################################
################################################################################


@app.route("/landlords")
def landlords():
    if request.args.get("landlord_id"):
        landlord_id = request.args.get("landlord_id")
        this_landlord = m.Landlord.objects.get(id=landlord_id)
        user_already_reviewed = False  # Default to false
        if g.user.is_active():  # If user is signed in
            this_user = m.User.objects().get(id=g.user.id)  # get user object
            user_already_reviewed = this_landlord.has_user_already_reviewed(this_user)
        return render_template("landlord_one.html",
                               landlord=this_landlord,
                               already_reviewed=user_already_reviewed,
                               nav=nav(cur_town=this_landlord.town))
    all_landlords = m.Landlord.objects()
    return redirect(url_for("towns"))


@app.route("/add_landlord", methods=['GET', 'POST'])
@login_required
def add_landlord():
    if request.method == 'POST':
        name = request.form.get("landlord_name")
        website = request.form.get("landlord_website")
        town_id = request.form.get("landlord_town")
        town_obj = m.Town.objects.get(id=town_id)
        this_user = m.User.objects().get(id=g.user.id)
        new_landlord = m.Landlord()
        new_landlord.create(name, website, town_obj, this_user)
        new_landlord.save()
        return url_for("towns", town_id=town_id)
    return render_template("landlord_new.html", towns=m.Town.objects(), nav=nav())

@app.route("/add_landlord_review", methods=['GET', 'POST'])
@login_required
def add_landlord_review():
    if request.method == 'POST':
        this_author = m.User.objects().get(id=g.user.id)
        landlord_id = request.form.get("review_landlord_id")
        this_landlord = m.Landlord.objects.get(id=landlord_id)
        # Check that this user hasn't already reviewed this landlord.
        # Users can only review/vote once per object
        if this_landlord.has_user_already_reviewed(this_author):
            return "Error - You have already reviewed this landlord.\nPlease edit your original review instead."

        title = request.form.get("review_title")
        body = request.form.get("review_body")
        timestamp = datetime.now()
        year = request.form.get("review_year")
        rating = request.form.get("review_rating")

        new_review = m.LandlordReview()
        new_review.create(title, body, timestamp, this_landlord, rating, this_author, year)
        new_review.save()
        # this_landlord.reviews.append(new_review)
        new_review.calc_average()
        this_landlord.save()
        return url_for("landlords", landlord_id=landlord_id)
    landlord_id = request.args.get("landlord_id")
    landlord = m.Landlord.objects.get(id=landlord_id)
    return render_template("landlord_review_new.html", nav=nav(), landlord=landlord)


################################################################################
#################### API #######################################################
################################################################################



@app.route("/api/vote_review", methods=["POST"])
def vote_review():
    """
    Upvote a review. Return whether upvote was successfull or not.
    Client-side js will handle updating number on page.
    """
    up_or_down = request.values.get("up_or_down")
    review_type = request.values.get("review_type")
    user_id = request.values.get("user_id")
    user = m.User.objects(id=user_id)
    user = None if len(user) != 1 else user[0]
    if not user:
        return "error - Invalid user"
    review_id = request.values.get("review_id")
    try:
        if review_type == "house":
            review = m.HouseReview.objects(id=review_id)[0]
        elif review_type == "landlord":
            review = m.LandlordReview.objects(id=review_id)[0]
        else:
            raise Exception("invalid_review_id")
    except:
        return "error - Invalid review"
    # Choose whether to up/down-vote based on input
    if "up" in up_or_down:
        result = review.upvote(user)
    else:
        result = review.downvote(user)
    if "error" in result:
        return result  # js will deal with "error"
    # if all ok, return number of voters, to be displayed
    nums = review.get_up_down_nums()
    return nums



@app.route("/api/delete_review", methods=['GET', 'POST'])
@login_required
def delete_review():
    if request.method == 'POST':
        review_type = request.values.get("review_type")  # house or landlord
        this_user = m.User.objects().get(id=g.user.id)
        review_id = request.form.get("review_id")
        if review_type == "house":
            this_review = m.HouseReview.objects.get(id=review_id)
        else:
            this_review = m.LandlordReview.objects.get(id=review_id)
        if this_user.role != 2:
            if not this_user == this_review.author:
                return "Error - you are not the author of this review"
        this_review.delete()
        this_review.save()
        return "ok"

@app.route("/edit_review", methods=['GET', 'POST'])
@login_required
def edit_review_form():
    if request.method == 'POST':
        review_id = request.values.get("review_id")
        obj_id = request.values.get("obj_id")
        review_type = request.values.get("review_type")
        if review_type == "house":
            this_review = m.HouseReview.objects.get(id=review_id)
            this_object = m.House.objects.get(id=obj_id)
        elif review_type == "landlord":
            this_review = m.LandlordReview.objects.get(id=review_id)
            this_object = m.Landlord.objects.get(id=obj_id)
        else:
            return "Error - invalid review type"
        this_author = m.User.objects().get(id=g.user.id)
        if not this_object.has_user_already_reviewed(this_author):
            return "Error - You haven't written a review of this."
        new_body = request.values.get("review_body")
        new_title = request.values.get("review_title")
        new_rating = request.values.get("review_rating")
        this_review.body = new_body
        this_review.body = new_body
        this_review.title = new_title
        this_review.rating = new_rating
        this_review.save()
        if review_type == "house":
            return url_for('houses', house_id=this_object.id)
        elif review_type == "landlord":
            return url_for('landlords', landlord_id=this_object.id)
    
    
    review_id = request.values.get("review_id")
    obj_id = request.values.get("obj_id")
    review_type = request.values.get("review_type")
    if review_type == "house":
        this_review = m.HouseReview.objects.get(id=review_id)
        this_object = m.House.objects.get(id=obj_id)
    elif review_type == "landlord":
        this_review = m.LandlordReview.objects.get(id=review_id)
        this_object = m.Landlord.objects.get(id=obj_id)
    else:
        return "Error - invalid review type"
    this_author = m.User.objects().get(id=g.user.id)
    if not this_object.has_user_already_reviewed(this_author):
        return "Error - You haven't written a review of this."
    return render_template("review_edit.html", review_type=review_type, review=this_review, obj=this_object, nav=nav())

    
    
@app.route("/api/edit_review", methods=['GET', 'POST'])
@login_required
def edit_review():
    if request.method == 'POST':
        review_id = request.form.get("review_id")
        review_type = request.values.get("review_type")  # house or landlord
        if review_type == "house":
            this_review = m.HouseReview.objects.get(id=review_id)
            house_id = request.form.get("review_house_id")
            this_object = m.House.objects.get(id=house_id)
        elif review_type == "landlord":
            this_review = m.LandlordReview.objects.get(id=review_id)
            landlord_id = request.form.get("review_landlord_id")
            this_object = m.Landlord.objects.get(id=landlord_id)
        else:
            return "error - invalid review type"
        this_author = m.User.objects().get(id=g.user.id)
        if not this_object.has_user_already_reviewed(this_author):
            return "error - You haven't written a review of this."
        title = request.form.get("review_title")
        body = request.form.get("review_body")
        timestamp = datetime.now()
        rating = request.form.get("review_rating")

        this_review.title = title
        this_review.body = body
        this_review.rating = rating
        this_review.timestamp = timestamp
        this_review.calc_average()
        this_review.save()
        return "OK"
    # Ideally this is POST only, from client js, but we may want to render a separate
    # page that lets user send this data
    review_type = request.values.get("review_type")  # house or landlord
    house_id = request.args.get("house_id")
    review_id = request.args.get("review_id")
    house = m.House.objects.get(id=house_id)
    if review_type == "house":
        this_review = m.HouseReview.objects.get(id=review_id)
    else:
        this_review = m.LandlordReview.objects.get(id=review_id)
    return render_template("house_review_edit.html", nav=nav(cur_town=house.town, cur_hood=house.neighbourhood), house=house, review=this_review)



@app.route("/api/postcode", methods=['GET'])
def api_postcode():
    """
    Call the external api for postcode data.
    This is done to get around the restriction on external requests in js.
    Return only the parts of data we need.
    """
    postcode_in = request.args.get("postcode")
    api_call = "http://www.uk-postcodes.com/postcode/"+postcode_in.replace(" ", "")+".json"
    print api_call
    try:
        result = get(api_call).json()  # convert to json
        print pformat(result)
        postcode = result.get('postcode')  # properly formatted
        neighbourhood = result.get('administrative').get('ward').get('title').capitalize()
        # TODO capitalize neighbourhood fully
        town = result.get('administrative').get('council').get('title').capitalize()
        lat = result.get('geo').get('lat')
        lng = result.get('geo').get('lng')
        # Need to get landlords applicable to this area
        landlords = []
        town_objs = m.Town.objects(name=town)
        print town_objs
        if len(town_objs)>0:
            print town_objs[0].name
            print town_objs[0].landlords
            for ll in town_objs[0].landlords:
                landlords.append((str(ll.id), ll.name))

        data = {'postcode':postcode, 'neighbourhood':neighbourhood, 'town':town, 'lat':lat, 'lng':lng, 'landlords':landlords}
        print pformat(data)
        return json.dumps(data)
    except Exception, e:
        print str(e)
        return json.dumps({})
    return


@app.route("/api/add_img", methods=['POST'])
def api_add_img():
    """
    Take a URL and save as link to house image.
    Needs validation by moderator before shown.
    """
    img_url = request.values.get("img_url")
    house_id = request.values.get("house_id")
    house = m.House.objects(id=house_id)[0]
    print house
    print img_url
    house.images.append(img_url)
    house.save()
    return "ok"


################################################################################
#################### Misc ######################################################
################################################################################

@app.route("/")
def index():
    """ Home page """
    return render_template("index.html", nav=nav())

@app.route("/start")
def start():
    """ Getting started landing page """
    return render_template("start.html", nav=nav())

@app.route("/blog")
def blog():
    """ Blog """
    blog_posts = m.BlogPost.objects(category="blog")
    dev_posts = m.BlogPost.objects(category="dev")
    admin = False
    admin = (g.user.is_authenticated() and g.user.role == m.ROLE_ADMIN)
    return render_template("blog.html", blog_posts=blog_posts, dev_posts=dev_posts, nav=nav(), admin=admin)

@app.route('/add_blog_post', methods=['GET', 'POST'])
@login_required
def add_blog_post():
    print g.user.role
    if g.user.role != m.ROLE_ADMIN:
        return "Not authorized"
    if request.method == 'POST':
        title = request.form.get("title")
        category = request.form.get("category")
        content = request.form.get("content")
        print title, category, content
        new_post = m.BlogPost()
        new_post.create(title, category, content)
        new_post.save()
        return redirect(url_for('blog'))
    return render_template("blog_post_new.html", nav=nav())

@app.route("/contact", methods=['GET', 'POST'])
def contact():
    """Show contact page or send support email"""
    if request.method == 'POST':
        support_title = request.form.get("title")
        support_content = request.form.get("content")
        support_author_name = g.user.nickname
        support_author_email = g.user.email
        # TODO - send email to support
        return "Thanks! We'll be in touch..."
    return render_template("contact.html", nav=nav())


@app.route("/about")
def about():
    """ About page """
    return render_template("about.html", nav=nav())

def nav(cur_town=None, cur_hood=None):
    """
    Create a dict of properties for the nav bar, based on our current location
    """
    nav = {}
    nav['user'] = g.user
    #nav['all_towns'] = get_all_towns_from_cache()  # Get a cached list of all towns - no need to repeatedly query db
    nav['all_towns'] = m.Town.objects()
    if not cur_town:  # Not in a town, so show all towns
        nav['cur_town'] = None
        # Not in a town, so don't know which neighbourhoods to show
        nav['show_hoods'] = False
    else:   # We know what town we're in
        nav['cur_town'] = cur_town  # So set this town
        nav['show_hoods'] = True
        #nav['all_..get.getget.getlocs'] = get_all_hoods_from_cache(town)
        nav['all_hoods'] = cur_town.neighbourhoods
        if not cur_hood:  # But we don't know what hood
            nav['cur_hood'] = None
        else:  # We know what hood we're in
            nav['cur_hood'] = cur_hood  # So set this hood
    # print nav
    return nav




if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)