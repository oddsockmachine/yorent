# YoRent #

Trying to find a house to rent sucks
We're going to make it better.

### About ###

YoRent is a website that aims to make it easier for renters to find quality places to live while avoiding run-down houses and unscrupulous landlords.

[www.yorent.co.uk](www.yorent.co.uk)