# Unit Tests

import unittest
import app.model as m



class Tester(unittest.TestCase):
    def setup(self):
        return
    def test_House_w_Landlord(self):
        houses = m.House.objects()
        num_houses = len(houses)
        my_house = m.House()
        my_house.name = "test house"
        my_house.overview = "test overview"
        my_house.postcode = "yo10 4du"
        my_house.save()
        houses = m.House.objects()
        self.assertEqual(len(houses), num_houses+1)
        test_house = m.House.objects().get(name="test house")
        self.assertEqual(test_house.name, "test house")
        self.assertEqual(test_house.overview, "test overview")
        self.assertEqual(test_house.postcode, "yo10 4du")
        test_house.delete()
        houses = m.House.objects()
        self.assertEqual(len(houses), num_houses)
        
    def test_House_w_Landlord(self):
        houses = m.House.objects()
        num_houses = len(houses)
        my_house = m.House()
        my_house.name = "test house"
        my_house.overview = "test overview"
        my_house.postcode = "yo10 4du"
        my_house.save()
        houses = m.House.objects()
        self.assertEqual(len(houses), num_houses+1)

        landlords = m.Landlord.objects()
        num_landlords = len(landlords)
        
        my_landlord = m.Landlord()
        my_landlord.name = "Bob the agent"
        my_landlord.save()

        landlords = m.Landlord.objects()
        self.assertEqual(len(landlords), num_landlords+1)

        my_house.landlord = my_landlord
        my_house.save()
        self.assertEqual(my_house.landlord.name, my_landlord.name)

        self.assertEqual(len(my_landlord.houses), 0)
        my_landlord.houses.append(my_house)
        my_landlord.save()
        self.assertEqual(len(my_landlord.houses), 1)

        my_house.delete()
        houses = m.House.objects()
        self.assertEqual(len(houses), num_houses)

        my_landlord.delete()
        landlords = m.Landlord.objects()
        self.assertEqual(len(landlords), num_landlords)
        return
    
    def test_House_w_Reviews(self):
        return
        houses = m.House.objects()
        num_houses = len(houses)
        my_house = m.House()
        my_house.name = "test house"
        my_house.overview = "test overview"
        my_house.postcode = "yo10 4du"
        my_house.save()
        houses = m.House.objects()
        self.assertEqual(len(houses), num_houses+1)


if __name__ == '__main__':
    unittest.main()
