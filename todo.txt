# swap start and index pages - don't want every click back to home to show the splash screen.
Editing review rating doesn't update calculated ratings on house/houses view

overhaul layout ala http://www.torchbox.com/sites/files/tbx/styles/ourwork_galery/public/mbamenu.png?itok=CRNOEoaF

--- Marketing ---
More Facebook
r/York
Nouse, Vision, TLP
YSS - after their backing of a union letting agent

--- Important ---
<<<<<<< HEAD
=======
edit review throws error
reviews icon should be speech bubble
reviews and images icons should link to correct tabs
use urlfield for images, weblinks etc
>>>>>>> b604e730314058700b693e99f5a2e960a0c0ee95
better "at a glance" info - when adding house, select number of beds, bathrooms, parking, garden etc. Calculate distance to city centre, station, universities etc.
use google maps to find centre lat/lng of new town
better way of finding houses - search, map etc?
move js from html to static/js
debug/staging server
unit-test all the things!
selenium
make years of reviews clearer, add to edit form

--- Superficial ---  (look nice, not functionally necessary)
use urlfield for images, weblinks etc
When showing lists of objects (houses, towns, landlords etc), show thumbnail on left, text on right, for better appearance and layout.
convert validation alerts into red boxes and side-by-side info messages
Should urls use routes/names? eg /towns/colchester, /towns/york/fishergate, instead of ?*_id=abcdef
sort neighbourhoods, towns, landlords by num properies, ratings etc
sort reviews by date, rating
    # stub buttons on page
    page ready, read in reviews as list of json objects, output in default order.
    On button press, clear list and output in new order
is there a reason that rating is not in BaseReview class?
Users adding new houses: flag for review by moderator.
Users adding new review: flag for review by moderator.
Users adding new landlords: send to admin, allow admin to post.


--- Big tasks ---  (todo if people consider them useful)
sort houses by distance to area
find houses within <x> miles of <postcode> - is this needed since we can search by neighbourhood?
how to get map to zoom to correct area when there are multiple markers? js?
Find housemates feature - added coming soon link. How will this work?
Notifications feature - how will this work? Allow LL/Agent to mark a house as empty/open for viewings. Send txt/email to all subscribed users.
consider packaging this up in Docker, for deployment by universities etc.
consider move towards reactive frontend with pure api backend


--- Done ---
# show houses on map using address lookup, not just postcode - more accurate (hopefully), stops stacking of nearby houses - use google geocoder to product latlng. Done, but had to geocode on house creation, not client viewing, to avoid hitting the rate limit. Will need to update lat/lng of existing houses (or at least those that have the same postcodes)
# redirect to previous page when logging in.  - does when login is required
# Can't add landlord review (ms)  - fixed copy-pasto with author/user.
# add house button reads submit review (ms)  - renamed
# alignment problem with house thumbnails on town page using Safari (jh) (and c/ff,when half window) - overflow=hidden fixes this.
# town-level map doesn't load properly (jh) - problem was with initializing map when tab was not active. Map is now not initalised on pageload, but once on tab click.
# unrated house has 0 stars, so appears ranked alongside 0-rated houses (ms) - shows not yet rated message
# Lots of incorrect plurals - 1 properties etc
# keep track of who adds what house/landlord - field added_by_user
# ensure edit function still works.
# when adding review, select year that review applies to.
# add captions to images <use dictField, like {url:caption, url:caption} - doesn't work, can't have . in key>
# More intuitive way of navigating away from house pages
# When adding new house, look up neighbourhood etc from postcode.
# use js button to call postcode info service, and show info in web forms.
# Validate town is correct, notify admin if not.
# If neighbourhood doesn't exist yet, create it, add house to nhood, add nhood to town.
# Find gps co-ords of postcode, save them - can be used to sort-by-distance.
# star ratings of landlords > 5
# star ratings
# town view - list -> links
# Show neighbourhoods on town page
# use remote image links
# submit image links from client
# carousel view at top of page
# hide carousel if no images
# put carousel in separate template
# delete nameless houses from db - consider how to delete references to things (ie house refs in town.houses)
# other/new category of landlord when adding house
# show landlords page per town, or show landlord ratings on town page
# when creating objects that are held by others (eg nhood in town), append obect to parent's list inside create() and save()
# get nav bar working
# new/other landlord option doesn't show landlord name field automatically
# town expand grids of houses, nhoods and llords
# expand/hide carousel - works, needs to look nicer
# Neighbourhood average rating of houses
# random duck pictures as placeholders ;)
# house_small, llord_small etc: small squares for embedding overview in larger pages
# Create users, with concept of user/moderator/admin
# store author in review
# when adding review, use stars (more intuitive and harder to break w incorrect numbers)
# rate landlords with stars too.
# when adding house, if there are no landlords available to select from, you cannot trigger the event to add a new one. Fixed: start with blank selection, gives warning if submitted with no landlord.
# clean up tables for review form
# user link on reviews
# CDN js/optimize static stuff
# review models should count num of upvotes, via list of who upvoted
# up/downvote like reddit
# rank reviews by up/downvotes
# show rank on review +/- different colours
# small review html subsection, since h/ll are identical and complex
# "notify me when this house becomes available" button - text or email
# "edit my review" incase you made a mistake.
# "upvote/downvote" on review
# "suggest an edit" on landlord/house page - so people can correct inaccuracies.
# refactor h/ll reviews to use base class!
# refactor api/vote to use single function for both h and ll
# example/fake images are now at app/static, not static
# put more info on neighbourhoods page, inc map w pins for all houses
# review helpfulness voting like reddit
# sort reviews by helpfulness
# nicer views
# pros/cons section on review - won't do, too much clutter
# sort houses, landlords by rating
# blog
# replace nav() with _render_template() - ignore, current method is more verbose but clearer
# town page should use tabs for houses/nhoods/llords
# investigate "before_request" operating on js loads - everything is now much slower - using CDN, not an issue now
# house page should use tabs for details, pics, location, reviews, landlord etc
# show map on house view
# timestamp on reviews - tried it, doesn't add anything
# better flashed message display
# bigger text input boxes for blog content, house overview, review content, contact content
# More explanation/instructions around user input forms
# hover tool-tips/info buttons - explain about safety, security etc.
# buttons on house thumbnails (photos, reviews) should link to correct tab on house view  - tried, but flask can't cope with objectid#tab format
# basic map functionality working.
# map on house page works
# added map to llord page, with markers for all houses. These markers should link to house
# map tab on town page, with markers for all houses - done but problem with layout???
# users should only be able to add one review per house/landlord.
# only one button: add review or edit review, depending on whether user has already reviewed.
# convert add house/ll form to use the same layout as add review.
# {% if house.images|length == 0}dis    play random duck(s){% else %}display real images{% endif %}
# external links (eg to landlords) don't work, are relative to root
# Delete/edit button on all views.
# Delete/edit buttons only visible by admins.
# validate form data on client side
# contact - use emails
# landlord page must be "owned by" a user - so the landlord can edit details
# pics must be links
# edit page/script for reviews/houses/landlords.
# When editing, make original text editable
# would showing reviews/ratings only to signed-up users encourage people to sign up? - not right now, better to have more users inputting data to start with.
# When adding new town/city, get top image from wikipedia, use as thumbnail on homepage
# Keep height of carousel images constant to prevent page jumping around
# fuck it, let's go!